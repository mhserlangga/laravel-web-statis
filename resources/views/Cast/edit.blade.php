@extends('layout.master')

@section('judul')
    Edit Cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
             <label>Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Title">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="bio" class="form-control" cols="30" rows="10" value="{{$cast->bio}}"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection